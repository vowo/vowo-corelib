package log

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"go.uber.org/zap"
	"go.uber.org/zap/zaptest/observer"
)

type LoggerTestSuite struct {
	suite.Suite
}

func (suite *LoggerTestSuite) TestLogging() {
	// arrange
	fac, logs := observer.New(zap.DebugLevel)
	zapLogger := zap.New(fac, zap.AddCaller(), zap.AddStacktrace(zap.DebugLevel))
	logger := NewFactory(zapLogger)

	// act
	logger.Bg().Info("test logging")

	//	assert
	assert.Equal(suite.T(), 1, logs.Len())

	entry := logs.TakeAll()[0].Entry
	callerInfo := entry.Caller.TrimmedPath()

	assert.Equal(suite.T(), "test logging", entry.Message)
	assert.Equal(suite.T(), "log/logger_test.go:23", callerInfo) //NOTE the line number changes if this test is adapted
}

func TestLoggerTestSuite(t *testing.T) {
	suite.Run(t, new(LoggerTestSuite))
}
