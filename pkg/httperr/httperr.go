package httperr

import (
	"encoding/json"
	"log"
	"net/http"
)

// HandleError checks if the error is not nil, writes it to the output
// with the specified status code, and returns true. If error is nil it returns false.
func HandleError(w http.ResponseWriter, err error, statusCode int) bool {
	if err == nil {
		return false
	}

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(statusCode)
	jsonErr := json.NewEncoder(w).Encode(map[string]interface{}{
		"error": err.Error(),
	})

	if jsonErr != nil {
		log.Fatalf("error starting microservice %v", jsonErr.Error())
	}

	return true
}

// HandleNotFound handles rest api not found
func HandleNotFound(w http.ResponseWriter, r *http.Request) {
	if r == nil || w == nil {
		panic(errArgumentException)
	}

	HandleError(w, ErrNotFound, http.StatusNotFound)
}

// HandleBadRequest handles rest api bad request
func HandleBadRequest(w http.ResponseWriter, r *http.Request) {
	if r == nil || w == nil {
		panic(errArgumentException)
	}

	HandleError(w, ErrBadRequest, http.StatusBadRequest)
}

// HandleMethodNotAllowed handles rest api method not allowed
func HandleMethodNotAllowed(w http.ResponseWriter, r *http.Request) {
	if r == nil || w == nil {
		panic(errArgumentException)
	}

	HandleError(w, ErrMethodNotAllowed, http.StatusMethodNotAllowed)
}
