package httperr

import "errors"

// ErrUnauthorized is returned when apiKey not valid.
var ErrUnauthorized = errors.New("unauthorized")

// ErrNotFound is returned when object is not found.
var ErrNotFound = errors.New("not found")

// ErrBadRequest is returned when an invalid request is issued.
var ErrBadRequest = errors.New("bad request")

// ErrMethodNotAllowed is returned when api does not support method
var ErrMethodNotAllowed = errors.New("method not allowed")

// ErrInternalServerError is returned when unexpected error occurs
var ErrInternalServerError = errors.New("internal server error")

var errArgumentException = errors.New("The argument provided is not valid")
