package httputils

import (
	"bytes"
	"context"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strings"

	"github.com/go-kit/kit/endpoint"
	"github.com/pkg/errors"
	"gitlab.com/vowo/vowo-corelib/pkg/httperr"
)

// HTTPPrefixURL prefixes an url with "http"
func HTTPPrefixURL(url string) string {
	if !strings.HasPrefix(url, "http") {
		url = "http://" + url
	}

	return url
}

func AddPathToURL(base *url.URL, path string) *url.URL {
	next := *base
	next.Path = path
	return &next
}

// EncodeHTTPGenericRequest is a transport/http.EncodeRequestFunc that
// JSON-encodes any request to the request body. Primarily useful in a client.
func EncodeHTTPGenericRequest(_ context.Context, r *http.Request, request interface{}) error {
	var buf bytes.Buffer
	if err := json.NewEncoder(&buf).Encode(request); err != nil {
		return err
	}

	r.Body = ioutil.NopCloser(&buf)
	return nil
}

// EncodeHTTPGenericResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer. Primarily useful in a server.
func EncodeHTTPGenericResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	if f, ok := response.(endpoint.Failer); ok && f.Failed() != nil {
		errorEncoder(ctx, f.Failed(), w)
		return nil
	}

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	return json.NewEncoder(w).Encode(response)
}

func errorEncoder(_ context.Context, err error, w http.ResponseWriter) {
	w.WriteHeader(err2code(err))
	json.NewEncoder(w).Encode(errorWrapper{Error: err.Error()})
}

func err2code(err error) int {
	switch err {
	case httperr.ErrBadRequest:
		return http.StatusBadRequest
	}

	return http.StatusInternalServerError
}

type errorWrapper struct {
	Error string `json:"error"`
}

// EncodeJSONResponse is a utility method that encodes the response as JSON to the response writer and handles write error
func EncodeJSONResponse(w http.ResponseWriter, response interface{}, statusCode int) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")

	data, err := json.Marshal(response)
	if err != nil {
		httperr.HandleError(w, errors.Wrap(httperr.ErrInternalServerError, "unable to encode JSON data"), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(statusCode)
	_, err = w.Write(data)
	if err != nil {
		log.Printf("could not write json %v", err.Error())
	}
}
