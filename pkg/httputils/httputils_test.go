package httputils

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"

	"github.com/opentracing-contrib/go-stdlib/nethttp"
	"github.com/stretchr/testify/suite"
)

type HTTPUtilsTestSuite struct {
	suite.Suite
}

type response struct {
	Name  string `json:"name"`
	Error string `json:"error,omitempty"`
}

var client = &http.Client{Transport: &nethttp.Transport{}}

func (suite *HTTPUtilsTestSuite) getJSON(url string, expectedStatus int, out interface{}) error {
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return err
	}

	res, err := client.Do(req)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	assert.Equal(suite.T(), expectedStatus, res.StatusCode)
	decoder := json.NewDecoder(res.Body)
	return decoder.Decode(out)
}

func (suite *HTTPUtilsTestSuite) TestEncodeResponse() {
	// arrange
	resp := &response{Name: "SomeName"}
	encodeStub := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		EncodeJSONResponse(w, resp, http.StatusOK)
	}))
	defer encodeStub.Close()

	// act
	actualResponse := &response{}
	err := suite.getJSON(encodeStub.URL, http.StatusOK, actualResponse)

	// assert
	assert.NoError(suite.T(), err)
	assert.Equal(suite.T(), resp.Name, actualResponse.Name)
}

func (suite *HTTPUtilsTestSuite) TestEncodeNil() {
	// arrange
	encodeStub := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		EncodeJSONResponse(w, nil, http.StatusOK)
	}))
	defer encodeStub.Close()

	// act
	actualResponse := &response{}
	err := suite.getJSON(encodeStub.URL, http.StatusOK, actualResponse)

	// assert
	assert.NoError(suite.T(), err)
	assert.Equal(suite.T(), "", actualResponse.Error)
	assert.Equal(suite.T(), "", actualResponse.Name)
}

func (suite *HTTPUtilsTestSuite) TestEncodeError() {
	// arrange
	encodeStub := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		EncodeJSONResponse(w, func() {}, http.StatusOK)
	}))
	defer encodeStub.Close()

	// act
	actualResponse := &response{}
	err := suite.getJSON(encodeStub.URL, http.StatusInternalServerError, actualResponse)

	// assert
	assert.NoError(suite.T(), err)
	assert.Equal(suite.T(), "unable to encode JSON data: internal server error", actualResponse.Error)
	assert.Equal(suite.T(), "", actualResponse.Name)
}

func TestHTTPUtilsTestSuite(t *testing.T) {
	suite.Run(t, new(HTTPUtilsTestSuite))
}
