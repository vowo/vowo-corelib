# vowo.io Corelib

A collection of shared infrastructure libraries used by different components of the vowo.io backend. This library is not intended to be used standalone, and provides no guarantees of backwards compatibility.

[![pipeline status](https://gitlab.com/vowo/vowo-corelib/badges/master/pipeline.svg)](https://gitlab.com/vowo/vowo-corelib/commits/master)

## Run it

```bash
dep ensure
go build -v ./...
go test -v ./...
```
